$(document).ready(() => {
  var managerTeam = [{
    "id": "m1",
    "text": "M1"
  }]

  $("#categories")
    .jstree({
      "core": {
        "data": managerTeam,
        "check_callback": true
      }
    })



  // Botão criar nós
  $('#create').click(function () {
    // Recupera o nome da pasta selecionada para criar a partir da selecao
    var sel = $.jstree.reference('#categories').get_selected(true)[0]

    // Cria a pasta com edit
    $("#categories").jstree("create_node", sel.id, null, "last", function (node) {
      this.edit(node)
    })
  })


  // Botão deletar pasta ou sub-pastas
  $('#deleteNode').click(function () {
    // Recupera o nome da pasta selecionada para deletar a partir da selecao
    var sel = $.jstree.reference('#categories').get_selected(true)[0];

    $("#categories").jstree("delete_node", sel.id)
  })

  // $(() => {
  //   $('#tree-view').jstree({
  //     'core': {
  //       'dataType': 'json',
  //       'url': '/data.json',
  //       'data': function (node) {
  //         console.log(node)
  //       }
  //     }
  //   })
  // })


})