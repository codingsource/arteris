var jsonData = [{
  id: 1,
  text: "Folder 1",
  state: {
    selected: false
  },
  children: [{
      id: 2,
      text: "Sub Folder 1",
      state: {
        selected: false
      },
    },
    {
      id: 3,
      text: "Sub Folder 2",
      state: {
        selected: false
      },
    }
  ]
},
{
  id: 4,
  text: "Folder 2",
  state: {
    selected: true
  },
  children: []
}
];

if (typeof module != "undefined" && module.exports)
module.exports = jsonData;